=======
rsd-lib
=======

Extended Sushy library for Rack Scale Design

This library extends the existing Sushy library to include functionality
for Intel RackScale Design enabled hardware. Capabilities include logical
node composition and decomposition, remote storage discovery and composition,
and NVMe over PCIe drive attaching and detaching to logical nodes.

It supports the API interface of RSD version 2.1, 2.2 and 2.3 now. Part of RSD
2.4 API have been supported, and the rest of them are still under
implementation.

* Free software: Apache license
* Documentation: https://opendev.org/x/rsd-lib/src/branch/master/doc/source/reference/usage.rst
* Source: https://opendev.org/x/rsd-lib
* Bugs: https://launchpad.net/python-rsdclient
