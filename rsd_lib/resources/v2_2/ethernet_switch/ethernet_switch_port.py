# Copyright 2018 Intel, Inc.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from sushy.resources import base
from sushy import utils

from rsd_lib.resources.v2_1.ethernet_switch import ethernet_switch_port
from rsd_lib.resources.v2_1.system import ethernet_interface
from rsd_lib.resources.v2_2.ethernet_switch import ethernet_switch_port_metrics
from rsd_lib import utils as rsd_lib_utils


class LinksField(base.CompositeField):

    primary_vlan = base.Field(
        "PrimaryVLAN", adapter=rsd_lib_utils.get_resource_identity
    )

    switch = base.Field("Switch", adapter=rsd_lib_utils.get_resource_identity)

    member_of_port = base.Field(
        "MemberOfPort", adapter=rsd_lib_utils.get_resource_identity
    )

    port_members = base.Field(
        "PortMembers", adapter=utils.get_members_identities
    )

    active_acls = base.Field(
        "ActiveACLs", adapter=utils.get_members_identities
    )


class EthernetSwitchPort(ethernet_switch_port.EthernetSwitchPort):

    links = LinksField("Links")

    @property
    @utils.cache_it
    def metrics(self):
        """Property to provide reference to `EthernetSwitchPortMetrics` instance

           It is calculated once when it is queried for the first time. On
           refresh, this property is reset.
        """
        return ethernet_switch_port_metrics.EthernetSwitchPortMetrics(
            self._conn,
            utils.get_sub_resource_path_by(self, "Metrics"),
            redfish_version=self.redfish_version,
        )

    @property
    @utils.cache_it
    def neighbor_interface(self):
        """Property to provide reference to `EthernetInterface` instance

           It is calculated once when it is queried for the first time. On
           refresh, this property is reset.
        """
        return ethernet_interface.EthernetInterface(
            self._conn,
            utils.get_sub_resource_path_by(self, "NeighborInterface"),
            redfish_version=self.redfish_version,
        )


class EthernetSwitchPortCollection(
    ethernet_switch_port.EthernetSwitchPortCollection
):
    @property
    def _resource_type(self):
        return EthernetSwitchPort
