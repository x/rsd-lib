# Copyright (c) 2019 Intel, Corp.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from sushy.resources import base

from rsd_lib import base as rsd_lib_base
from rsd_lib import utils as rsd_lib_utils


class VolumeMetrics(rsd_lib_base.ResourceBase):

    name = base.Field("Name")
    """The volume metrics name"""

    description = base.Field("Description")
    """The volume metrics description"""

    identity = base.Field("Id", required=True)
    """The volume metrics identity string"""

    capacity_used_bytes = base.Field(
        "CapacityUsedBytes", adapter=rsd_lib_utils.num_or_none
    )
    """The capacity in bytes of the volume"""
