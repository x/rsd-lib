# Copyright 2017 Intel, Inc.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import json
import jsonschema
import mock
import testtools

from sushy import exceptions

from rsd_lib import constants
from rsd_lib.resources.v2_1.node import node
from rsd_lib.tests.unit.fakes import request_fakes


class NodeTestCase(testtools.TestCase):
    def setUp(self):
        super(NodeTestCase, self).setUp()
        self.conn = mock.Mock()
        with open("rsd_lib/tests/unit/json_samples/v2_1/node.json", "r") as f:
            self.conn.get.return_value.json.return_value = json.loads(f.read())

        self.node_inst = node.Node(
            self.conn, "/redfish/v1/Nodes/Node1", redfish_version="1.0.2"
        )

    def test__parse_attributes(self):
        self.assertEqual("1.0.2", self.node_inst.redfish_version)
        self.assertEqual("Node #1", self.node_inst.description)
        self.assertEqual("Allocated", self.node_inst.composed_node_state)
        self.assertEqual("Node1", self.node_inst.identity)
        self.assertEqual("Composed Node", self.node_inst.name)
        self.assertEqual(
            "fa39d108-7d70-400a-9db2-6940375c31c2", self.node_inst.uuid
        )
        self.assertEqual("On", self.node_inst.power_state)
        self.assertEqual("Enabled", self.node_inst.status.state)
        self.assertEqual("OK", self.node_inst.status.health)
        self.assertEqual("OK", self.node_inst.status.health_rollup)
        self.assertEqual(32, self.node_inst.memory.total_system_memory_gib)
        self.assertEqual("Enabled", self.node_inst.memory.status.state)
        self.assertEqual("OK", self.node_inst.memory.status.health)
        self.assertEqual("OK", self.node_inst.memory.status.health_rollup)
        self.assertEqual(2, self.node_inst.processors.count)
        self.assertEqual(
            "Multi-Core Intel(R) Xeon(R) processor 7xxx Series",
            self.node_inst.processors.model,
        )
        self.assertEqual("Enabled", self.node_inst.processors.status.state)
        self.assertEqual("OK", self.node_inst.processors.status.health)
        self.assertEqual("OK", self.node_inst.processors.status.health_rollup)
        self.assertEqual(
            "/redfish/v1/Systems/System1", self.node_inst.links.computer_system
        )
        self.assertEqual(
            ("/redfish/v1/Systems/System1/Processors/CPU1",),
            self.node_inst.links.processors,
        )
        self.assertEqual(
            ("/redfish/v1/Systems/System1/Memory/Dimm1",),
            self.node_inst.links.memory,
        )
        self.assertEqual(
            ("/redfish/v1/Systems/System1/EthernetInterfaces/LAN1",),
            self.node_inst.links.ethernet_interfaces,
        )
        self.assertEqual(
            ("/redfish/v1/Chassis/Blade1/Drives/1",),
            self.node_inst.links.local_drives,
        )
        self.assertEqual(
            ("/redfish/v1/Services/RSS1/Targets/target1",),
            self.node_inst.links.remote_drives,
        )

    def test__parse_attributes_missing_actions(self):
        self.node_inst.json.pop("Actions")
        self.assertRaisesRegex(
            exceptions.MissingAttributeError,
            "attribute Actions",
            self.node_inst.refresh,
        )

    def test__parse_attributes_missing_reset_target(self):
        self.node_inst.json["Actions"]["#ComposedNode.Reset"].pop("target")
        self.assertRaisesRegex(
            exceptions.MissingAttributeError,
            "attribute Actions/#ComposedNode.Reset/target",
            self.node_inst.refresh,
        )

    def test_get__reset_action_element(self):
        value = self.node_inst._get_reset_action_element()
        self.assertEqual(
            "/redfish/v1/Nodes/Node1/Actions/ComposedNode.Reset",
            value.target_uri,
        )
        self.assertEqual(
            [
                "On",
                "ForceOff",
                "GracefulRestart",
                "ForceRestart",
                "Nmi",
                "ForceOn",
                "PushPowerButton",
                "GracefulShutdown",
            ],
            value.allowed_values,
        )

    def test__get_reset_action_element_missing_reset_action(self):
        self.node_inst._actions.reset = None
        self.assertRaisesRegex(
            exceptions.MissingActionError,
            "action #ComposedNode.Reset",
            self.node_inst._get_reset_action_element,
        )

    def test__get_assemble_action_element(self):
        value = self.node_inst._get_assemble_action_element()
        self.assertEqual(
            "/redfish/v1/Nodes/Node1/Actions/ComposedNode.Assemble",
            value.target_uri,
        )

    def test__get_attach_endpoint_action_element(self):
        value = self.node_inst._get_attach_endpoint_action_element()
        self.assertEqual(
            "/redfish/v1/Nodes/Node1/Actions/ComposedNode.AttachEndpoint",
            value.target_uri,
        )

        self.assertEqual(
            (
                "/redfish/v1/Chassis/PCIeSwitchChassis/Drives/Disk.Bay.1",
                "/redfish/v1/Chassis/PCIeSwitchChassis/Drives/Disk.Bay.2",
            ),
            value.allowed_values,
        )

    def test__get_detach_endpoint_action_element(self):
        value = self.node_inst._get_detach_endpoint_action_element()
        self.assertEqual(
            "/redfish/v1/Nodes/Node1/Actions/ComposedNode.DetachEndpoint",
            value.target_uri,
        )

        self.assertEqual(
            tuple(["/redfish/v1/Chassis/PCIeSwitchChassis/Drives/Disk.Bay.3"]),
            value.allowed_values,
        )

    def test_get_allowed_reset_node_values(self):
        values = self.node_inst.get_allowed_reset_node_values()
        expected = set(
            [
                "On",
                "ForceOff",
                "GracefulShutdown",
                "GracefulRestart",
                "ForceRestart",
                "Nmi",
                "ForceOn",
                "PushPowerButton",
            ]
        )
        self.assertEqual(expected, values)
        self.assertIsInstance(values, set)

    @mock.patch.object(node.LOG, "warning", autospec=True)
    def test_get_allowed_reset_node_values_no_values_specified(self, mock_log):
        self.node_inst._actions.reset.allowed_values = {}
        values = self.node_inst.get_allowed_reset_node_values()
        # Assert it returns all values if it can't get the specific ones
        expected = set(constants.RESET_TYPE_VALUE)
        self.assertEqual(expected, values)
        self.assertIsInstance(values, set)
        self.assertEqual(1, mock_log.call_count)

    def test_reset_node(self):
        self.node_inst.reset_node("ForceOff")
        self.node_inst._conn.post.assert_called_once_with(
            "/redfish/v1/Nodes/Node1/Actions/ComposedNode.Reset",
            data={"ResetType": "ForceOff"},
        )

    def test_reset_node_invalid_value(self):
        with self.assertRaisesRegex(
            exceptions.InvalidParameterValueError,
            'The parameter "value" value "invalid-value" is invalid',
        ):
            self.node_inst.reset_node("invalid-value")

    def test_get_allowed_node_boot_source_values(self):
        values = self.node_inst.get_allowed_node_boot_source_values()
        expected = set(["None", "Pxe", "Hdd", "RemoteDrive"])
        self.assertEqual(expected, values)
        self.assertIsInstance(values, set)

    @mock.patch.object(node.LOG, "warning", autospec=True)
    def test_get_allowed_node_boot_source_values_no_values_specified(
        self, mock_log
    ):
        self.node_inst.boot.boot_source_override_target_allowed_values = None
        values = self.node_inst.get_allowed_node_boot_source_values()
        # Assert it returns all values if it can't get the specific ones
        expected = set(constants.BOOT_SOURCE_TARGET_VALUE)
        self.assertEqual(expected, values)
        self.assertIsInstance(values, set)
        self.assertEqual(1, mock_log.call_count)

    def test_get_allowed_node_boot_mode_values(self):
        values = self.node_inst.get_allowed_node_boot_mode_values()
        expected = set(["Legacy", "UEFI"])
        self.assertEqual(expected, values)
        self.assertIsInstance(values, set)

    @mock.patch.object(node.LOG, "warning", autospec=True)
    def test_get_allowed_node_boot_mode_values_no_values_specified(
        self, mock_log
    ):
        self.node_inst.boot.boot_source_override_mode_allowed_values = None
        values = self.node_inst.get_allowed_node_boot_mode_values()
        # Assert it returns all values if it can't get the specific ones
        expected = set(constants.BOOT_SOURCE_MODE_VALUE)
        self.assertEqual(expected, values)
        self.assertIsInstance(values, set)
        self.assertEqual(1, mock_log.call_count)

    def test_set_node_boot_source(self):
        self.node_inst.set_node_boot_source(
            target="Pxe", enabled="Continuous", mode="UEFI"
        )
        self.node_inst._conn.patch.assert_called_once_with(
            "/redfish/v1/Nodes/Node1",
            data={
                "Boot": {
                    "BootSourceOverrideEnabled": "Continuous",
                    "BootSourceOverrideTarget": "Pxe",
                    "BootSourceOverrideMode": "UEFI",
                }
            },
        )

    def test_set_node_boot_source_no_mode_specified(self):
        self.node_inst.set_node_boot_source(target="Hdd")
        self.node_inst._conn.patch.assert_called_once_with(
            "/redfish/v1/Nodes/Node1",
            data={
                "Boot": {
                    "BootSourceOverrideEnabled": "Once",
                    "BootSourceOverrideTarget": "Hdd",
                }
            },
        )

    def test_set_node_boot_source_invalid_target(self):
        with self.assertRaisesRegex(
            exceptions.InvalidParameterValueError,
            'The parameter "target" value "invalid-target" is invalid',
        ):
            self.node_inst.set_node_boot_source("invalid-target")

    def test_set_node_boot_source_invalid_enabled(self):
        with self.assertRaisesRegex(
            exceptions.InvalidParameterValueError,
            'The parameter "enabled" value "invalid-enabled" is invalid',
        ):
            self.node_inst.set_node_boot_source(
                "Hdd", enabled="invalid-enabled"
            )

    def test_set_node_boot_source_invalid_mode(self):
        with self.assertRaisesRegex(
            exceptions.InvalidParameterValueError,
            'The parameter "mode" value "invalid-mode" is invalid',
        ):
            self.node_inst.set_node_boot_source("Hdd", mode="invalid-mode")

    def test_assemble_node(self):
        self.node_inst.assemble_node()
        self.node_inst._conn.post.assert_called_once_with(
            "/redfish/v1/Nodes/Node1/Actions/ComposedNode.Assemble"
        )

    def test_get_allowed_attach_endpoints(self):
        expected = self.node_inst.get_allowed_attach_endpoints()
        result = (
            "/redfish/v1/Chassis/PCIeSwitchChassis/Drives/Disk.Bay.1",
            "/redfish/v1/Chassis/PCIeSwitchChassis/Drives/Disk.Bay.2",
        )
        self.assertEqual(expected, result)

        (
            self.node_inst._json["Actions"]["#ComposedNode.AttachEndpoint"][
                "Resource@Redfish.AllowableValues"
            ]
        ) = []
        self.node_inst.refresh()
        expected = self.node_inst.get_allowed_attach_endpoints()
        result = ()
        self.assertEqual(expected, result)

    def test_attach_endpoint(self):
        self.node_inst.attach_endpoint(
            endpoint="/redfish/v1/Chassis/PCIeSwitchChassis/Drives/Disk.Bay.1",
            capacity=100,
        )
        self.node_inst._conn.post.assert_called_once_with(
            "/redfish/v1/Nodes/Node1/Actions/ComposedNode.AttachEndpoint",
            data={
                "Resource": {
                    "@odata.id": "/redfish/v1/Chassis/"
                    "PCIeSwitchChassis/Drives/Disk.Bay.1"
                },
                "CapacityGiB": 100,
            },
        )

    def test_attach_endpoint_invalid_parameter(self):
        self.assertRaises(
            exceptions.InvalidParameterValueError,
            self.node_inst.attach_endpoint,
            endpoint="invalid",
        )

    def test_attach_endpoint_only_with_capacity_parameter(self):
        self.node_inst.attach_endpoint(capacity=100)
        self.node_inst._conn.post.assert_called_once_with(
            "/redfish/v1/Nodes/Node1/Actions/ComposedNode.AttachEndpoint",
            data={"CapacityGiB": 100},
        )

    def test_get_allowed_detach_endpoints(self):
        expected = self.node_inst.get_allowed_detach_endpoints()
        result = ("/redfish/v1/Chassis/PCIeSwitchChassis/Drives/Disk.Bay.3",)
        self.assertEqual(expected, result)

        (
            self.node_inst._json["Actions"]["#ComposedNode.DetachEndpoint"][
                "Resource@Redfish.AllowableValues"
            ]
        ) = []
        self.node_inst.refresh()
        expected = self.node_inst.get_allowed_detach_endpoints()
        result = ()
        self.assertEqual(expected, result)

    def test_detach_endpoint(self):
        self.node_inst.detach_endpoint(
            endpoint="/redfish/v1/Chassis/PCIeSwitchChassis/Drives/Disk.Bay.3"
        )
        self.node_inst._conn.post.assert_called_once_with(
            "/redfish/v1/Nodes/Node1/Actions/ComposedNode.DetachEndpoint",
            data={
                "Resource": "/redfish/v1/Chassis/PCIeSwitchChassis/"
                "Drives/Disk.Bay.3"
            },
        )

    def test_detach_endpoint_invalid_parameter(self):
        self.assertRaises(
            exceptions.InvalidParameterValueError,
            self.node_inst.detach_endpoint,
            endpoint="invalid",
        )

    def test_memory_summary_missing_attr(self):
        # | GIVEN |
        self.node_inst._json["Memory"]["Status"].pop("Health")
        # | WHEN |
        self.node_inst.refresh()
        # | THEN |
        self.assertEqual(32, self.node_inst.memory.total_system_memory_gib)
        self.assertEqual(None, self.node_inst.memory.status.health)
        self.assertEqual("Enabled", self.node_inst.memory.status.state)
        self.assertEqual("OK", self.node_inst.memory.status.health_rollup)

        # | GIVEN |
        self.node_inst._json["Memory"]["Status"].pop("State")
        # | WHEN |
        self.node_inst.refresh()
        # | THEN |
        self.assertEqual(32, self.node_inst.memory.total_system_memory_gib)
        self.assertEqual(None, self.node_inst.memory.status.health)
        self.assertEqual(None, self.node_inst.memory.status.state)
        self.assertEqual("OK", self.node_inst.memory.status.health_rollup)

        # | GIVEN |
        self.node_inst._json["Memory"]["Status"].pop("HealthRollup")
        # | WHEN |
        self.node_inst.refresh()
        # | THEN |
        self.assertEqual(32, self.node_inst.memory.total_system_memory_gib)
        self.assertEqual(None, self.node_inst.memory.status.health)
        self.assertEqual(None, self.node_inst.memory.status.state)
        self.assertEqual(None, self.node_inst.memory.status.health_rollup)

        # | GIVEN |
        self.node_inst._json["Memory"].pop("Status")
        # | WHEN |
        self.node_inst.refresh()
        # | THEN |
        self.assertEqual(32, self.node_inst.memory.total_system_memory_gib)
        self.assertEqual(None, self.node_inst.memory.status)

        # | GIVEN |
        self.node_inst._json["Memory"].pop("TotalSystemMemoryGiB")
        # | WHEN |
        self.node_inst.refresh()
        # | THEN |
        self.assertEqual(None, self.node_inst.memory.total_system_memory_gib)
        self.assertEqual(None, self.node_inst.memory.status)

        # | GIVEN |
        self.node_inst._json.pop("Memory")
        # | WHEN |
        self.node_inst.refresh()
        # | THEN |
        self.assertEqual(None, self.node_inst.memory)

    def test_delete_node(self):
        self.node_inst.delete_node()
        self.node_inst._conn.delete.assert_called_once_with(
            self.node_inst.path
        )


class NodeCollectionTestCase(testtools.TestCase):
    def setUp(self):
        super(NodeCollectionTestCase, self).setUp()
        self.conn = mock.Mock()
        with open(
            "rsd_lib/tests/unit/json_samples/v2_1/node_collection.json", "r"
        ) as f:
            self.conn.get.return_value = request_fakes.fake_request_get(
                json.loads(f.read())
            )
            self.conn.post.return_value = request_fakes.fake_request_post(
                None,
                headers={
                    "Location": "https://localhost:8443/redfish/v1/Nodes/1"
                },
            )
        self.node_col = node.NodeCollection(
            self.conn, "/redfish/v1/Nodes", redfish_version="1.0.2"
        )

    def test__parse_attributes(self):
        self.assertEqual("1.0.2", self.node_col.redfish_version)
        self.assertEqual("Composed Nodes Collection", self.node_col.name)
        self.assertEqual(
            ("/redfish/v1/Nodes/Node1",), self.node_col.members_identities
        )

    @mock.patch.object(node, "Node", autospec=True)
    def test_get_member(self, mock_node):
        self.node_col.get_member("/redfish/v1/Nodes/Node1")
        mock_node.assert_called_once_with(
            self.node_col._conn,
            "/redfish/v1/Nodes/Node1",
            redfish_version=self.node_col.redfish_version,
            registries=None,
        )

    @mock.patch.object(node, "Node", autospec=True)
    def test_get_members(self, mock_node):
        members = self.node_col.get_members()
        mock_node.assert_called_once_with(
            self.node_col._conn,
            "/redfish/v1/Nodes/Node1",
            redfish_version=self.node_col.redfish_version,
            registries=None,
        )
        self.assertIsInstance(members, list)
        self.assertEqual(1, len(members))

    def test__get_compose_action_element(self):
        value = self.node_col._get_compose_action_element()
        self.assertEqual(
            "/redfish/v1/Nodes/Actions/Allocate", value.target_uri
        )

    def test_compose_node_no_reqs(self):
        result = self.node_col.compose_node()
        self.node_col._conn.post.assert_called_once_with(
            "/redfish/v1/Nodes/Actions/Allocate", data={}
        )
        self.assertEqual(result, "/redfish/v1/Nodes/1")

    def test_compose_node_reqs(self):
        reqs = {
            "Name": "test",
            "Description": "this is a test node",
            "Processors": [
                {
                    "TotalCores": 4,
                    "Oem": {"Brand": "E7", "Capabilities": ["sse"]},
                }
            ],
            "Memory": [{"CapacityMiB": 8000}],
            "TotalSystemCoreCount": 8,
            "TotalSystemMemoryMiB": 16000,
        }
        result = self.node_col.compose_node(
            name="test",
            description="this is a test node",
            processor_req=[
                {
                    "TotalCores": 4,
                    "Oem": {"Brand": "E7", "Capabilities": ["sse"]},
                }
            ],
            memory_req=[{"CapacityMiB": 8000}],
            total_system_core_req=8,
            total_system_memory_req=16000,
        )
        self.node_col._conn.post.assert_called_once_with(
            "/redfish/v1/Nodes/Actions/Allocate", data=reqs
        )
        self.assertEqual(result, "/redfish/v1/Nodes/1")

    def test_compose_node_invalid_reqs(self):
        self.assertRaises(
            jsonschema.exceptions.ValidationError,
            self.node_col.compose_node,
            processor_req="invalid",
        )

        # Wrong processor Oem Brand
        with self.assertRaisesRegex(
            jsonschema.exceptions.ValidationError,
            ("'Platinum' is not one of \['E3', 'E5'"),
        ):

            self.node_col.compose_node(
                name="test",
                description="this is a test node",
                processor_req=[
                    {
                        "TotalCores": 4,
                        "Oem": {"Brand": "Platinum", "Capabilities": ["sse"]},
                    }
                ],
            )

        # Wrong processor Oem Capabilities
        with self.assertRaisesRegex(
            jsonschema.exceptions.ValidationError,
            ("'sse' is not of type 'array'"),
        ):

            self.node_col.compose_node(
                name="test",
                description="this is a test node",
                processor_req=[
                    {
                        "TotalCores": 4,
                        "Oem": {"Brand": "E3", "Capabilities": "sse"},
                    }
                ],
            )

        # Wrong processor Oem Capabilities
        with self.assertRaisesRegex(
            jsonschema.exceptions.ValidationError,
            ("0 is not of type 'string'"),
        ):

            self.node_col.compose_node(
                name="test",
                description="this is a test node",
                processor_req=[
                    {
                        "TotalCores": 4,
                        "Oem": {"Brand": "E3", "Capabilities": [0]},
                    }
                ],
            )
