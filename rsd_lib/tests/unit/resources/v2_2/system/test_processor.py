# Copyright 2018 Intel, Inc.
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import json
import mock
import testtools

from rsd_lib.resources.v2_2.system import processor
from rsd_lib.resources.v2_2.system import processor_metrics


class ProcessorTestCase(testtools.TestCase):
    def setUp(self):
        super(ProcessorTestCase, self).setUp()
        self.conn = mock.Mock()
        with open(
            "rsd_lib/tests/unit/json_samples/v2_2/processor.json", "r"
        ) as f:
            self.conn.get.return_value.json.return_value = json.loads(f.read())

        self.processor_inst = processor.Processor(
            self.conn,
            "/redfish/v1/Systems/System1/Processors/CPU1",
            redfish_version="1.1.0",
        )

    def test__parse_attributes(self):
        self.assertEqual("1.1.0", self.processor_inst.redfish_version)
        self.assertEqual("CPU1", self.processor_inst.identity)
        self.assertEqual("Processor", self.processor_inst.name)
        self.assertEqual(None, self.processor_inst.description)
        self.assertEqual("CPU 1", self.processor_inst.socket)
        self.assertEqual("CPU", self.processor_inst.processor_type)
        self.assertEqual("x86", self.processor_inst.processor_architecture)
        self.assertEqual("x86-64", self.processor_inst.instruction_set)
        self.assertEqual(
            "Intel(R) Corporation", self.processor_inst.manufacturer
        )
        self.assertEqual(
            "Multi-Core Intel(R) Xeon(R) processor 7xxx Series",
            self.processor_inst.model,
        )
        self.assertEqual(3700, self.processor_inst.max_speed_mhz)
        self.assertEqual(
            "0x42", self.processor_inst.processor_id.effective_family
        )
        self.assertEqual(
            "0x61", self.processor_inst.processor_id.effective_model
        )
        self.assertEqual(
            "0x34AC34DC8901274A",
            self.processor_inst.processor_id.identification_registers,
        )
        self.assertEqual(
            "0x429943", self.processor_inst.processor_id.microcode_info
        )
        self.assertEqual("0x1", self.processor_inst.processor_id.step)
        self.assertEqual(
            "GenuineIntel", self.processor_inst.processor_id.vendor_id
        )
        self.assertEqual("OK", self.processor_inst.status.health)
        self.assertEqual("OK", self.processor_inst.status.health_rollup)
        self.assertEqual("Enabled", self.processor_inst.status.state)
        self.assertEqual(8, self.processor_inst.total_cores)
        self.assertEqual(16, self.processor_inst.total_threads)
        self.assertEqual("E5", self.processor_inst.oem.intel_rackscale.brand)
        self.assertEqual(
            ["sse", "sse2", "sse3"],
            self.processor_inst.oem.intel_rackscale.capabilities,
        )
        self.assertEqual(
            "L2Cache",
            self.processor_inst.oem.intel_rackscale.on_package_memory[0].type,
        )
        self.assertEqual(
            2,
            self.processor_inst.oem.intel_rackscale.on_package_memory[
                0
            ].capacity_mb,
        )
        self.assertEqual(
            None,
            self.processor_inst.oem.intel_rackscale.on_package_memory[
                0
            ].speed_mhz,
        )
        self.assertEqual(
            "L3Cache",
            self.processor_inst.oem.intel_rackscale.on_package_memory[1].type,
        )
        self.assertEqual(
            20,
            self.processor_inst.oem.intel_rackscale.on_package_memory[
                1
            ].capacity_mb,
        )
        self.assertEqual(
            None,
            self.processor_inst.oem.intel_rackscale.on_package_memory[
                1
            ].speed_mhz,
        )
        self.assertEqual(
            160,
            self.processor_inst.oem.intel_rackscale.thermal_design_power_watt,
        )
        self.assertEqual(
            "/redfish/v1/Systems/System1/Processors/CPU1/Metrics",
            self.processor_inst.oem.intel_rackscale.metrics,
        )
        self.assertEqual(
            "0x0429943FFFFFFFFF",
            self.processor_inst.oem.intel_rackscale.
            extended_identification_registers.eax_00h,
        )
        self.assertEqual(
            "0x0429943FFFFFFFFF",
            self.processor_inst.oem.intel_rackscale.
            extended_identification_registers.eax_01h,
        )
        self.assertEqual(
            "0x0429943FFFFFFFFF",
            self.processor_inst.oem.intel_rackscale.
            extended_identification_registers.eax_02h,
        )
        self.assertEqual(
            "0x0429943FFFFFFFFF",
            self.processor_inst.oem.intel_rackscale.
            extended_identification_registers.eax_03h,
        )
        self.assertEqual(
            "0x0429943FFFFFFFFF",
            self.processor_inst.oem.intel_rackscale.
            extended_identification_registers.eax_04h,
        )
        self.assertEqual(
            "0x0429943FFFFFFFFF",
            self.processor_inst.oem.intel_rackscale.
            extended_identification_registers.eax_05h,
        )
        self.assertEqual(
            "0x0429943FFFFFFFFF",
            self.processor_inst.oem.intel_rackscale.
            extended_identification_registers.eax_07h,
        )
        self.assertEqual(
            "0x0429943FFFFFFFFF",
            self.processor_inst.oem.intel_rackscale.
            extended_identification_registers.eax_80000000h,
        )
        self.assertEqual(
            "0x0429943FFFFFFFFF",
            self.processor_inst.oem.intel_rackscale.
            extended_identification_registers.eax_80000001h,
        )
        self.assertEqual(
            "0x0429943FFFFFFFFF",
            self.processor_inst.oem.intel_rackscale.
            extended_identification_registers.eax_80000002h,
        )
        self.assertEqual(
            "0x0429943FFFFFFFFF",
            self.processor_inst.oem.intel_rackscale.
            extended_identification_registers.eax_80000003h,
        )
        self.assertEqual(
            "0x0429943FFFFFFFFF",
            self.processor_inst.oem.intel_rackscale.
            extended_identification_registers.eax_80000004h,
        )
        self.assertEqual(
            "0x0429943FFFFFFFFF",
            self.processor_inst.oem.intel_rackscale.
            extended_identification_registers.eax_80000005h,
        )
        self.assertEqual(
            "0x0429943FFFFFFFFF",
            self.processor_inst.oem.intel_rackscale.
            extended_identification_registers.eax_80000006h,
        )
        self.assertEqual(
            "0x0429943FFFFFFFFF",
            self.processor_inst.oem.intel_rackscale.
            extended_identification_registers.eax_80000007h,
        )
        self.assertEqual(
            "0x0429943FFFFFFFFF",
            self.processor_inst.oem.intel_rackscale.
            extended_identification_registers.eax_80000008h,
        )
        self.assertEqual(
            "Integrated", self.processor_inst.oem.intel_rackscale.fpga.type
        )
        self.assertEqual(
            "Blue1",
            self.processor_inst.oem.intel_rackscale.fpga.bit_stream_version,
        )
        self.assertEqual(
            "4x10G",
            self.processor_inst.oem.intel_rackscale.fpga.hssi_configuration,
        )
        self.assertEqual(
            "I2C", self.processor_inst.oem.intel_rackscale.fpga.hssi_sideband
        )
        self.assertEqual(
            1,
            self.processor_inst.oem.intel_rackscale.fpga.reconfiguration_slots,
        )

    def test_metrics(self):
        # | GIVEN |
        self.conn.get.return_value.json.reset_mock()
        with open(
            "rsd_lib/tests/unit/json_samples/v2_2/processor_metrics.json",
            "r",
        ) as f:
            self.conn.get.return_value.json.return_value = json.loads(f.read())
        # | WHEN |
        actual_metrics = self.processor_inst.metrics
        # | THEN |
        self.assertIsInstance(
            actual_metrics, processor_metrics.ProcessorMetrics
        )
        self.conn.get.return_value.json.assert_called_once_with()

        # reset mock
        self.conn.get.return_value.json.reset_mock()
        # | WHEN & THEN |
        # tests for same object on invoking subsequently
        self.assertIs(actual_metrics, self.processor_inst.metrics)
        self.conn.get.return_value.json.assert_not_called()

    def test_metrics_on_refresh(self):
        # | GIVEN |
        with open(
            "rsd_lib/tests/unit/json_samples/v2_2/processor_metrics.json",
            "r",
        ) as f:
            self.conn.get.return_value.json.return_value = json.loads(f.read())
        # | WHEN & THEN |
        self.assertIsInstance(
            self.processor_inst.metrics, processor_metrics.ProcessorMetrics
        )

        # On refreshing the processor instance...
        with open(
            "rsd_lib/tests/unit/json_samples/v2_2/processor.json", "r"
        ) as f:
            self.conn.get.return_value.json.return_value = json.loads(f.read())

        self.processor_inst.invalidate()
        self.processor_inst.refresh(force=False)

        # | GIVEN |
        with open(
            "rsd_lib/tests/unit/json_samples/v2_2/processor_metrics.json",
            "r",
        ) as f:
            self.conn.get.return_value.json.return_value = json.loads(f.read())
        # | WHEN & THEN |
        self.assertIsInstance(
            self.processor_inst.metrics, processor_metrics.ProcessorMetrics
        )


class ProcessorCollectionTestCase(testtools.TestCase):
    def setUp(self):
        super(ProcessorCollectionTestCase, self).setUp()
        self.conn = mock.Mock()
        with open(
            "rsd_lib/tests/unit/json_samples/v2_2/"
            "processor_collection.json",
            "r",
        ) as f:
            self.conn.get.return_value.json.return_value = json.loads(f.read())
        self.processor_col = processor.ProcessorCollection(
            self.conn,
            "/redfish/v1/Systems/System1/Processors",
            redfish_version="1.1.0",
        )

    def test__parse_attributes(self):
        self.assertEqual("1.1.0", self.processor_col.redfish_version)
        self.assertEqual(
            (
                "/redfish/v1/Systems/System1/Processors/CPU1",
                "/redfish/v1/Systems/System1/Processors/CPU2",
            ),
            self.processor_col.members_identities,
        )

    @mock.patch.object(processor, "Processor", autospec=True)
    def test_get_member(self, mock_system):
        self.processor_col.get_member(
            "/redfish/v1/Systems/System1/Processors/CPU1"
        )
        mock_system.assert_called_once_with(
            self.processor_col._conn,
            "/redfish/v1/Systems/System1/Processors/CPU1",
            redfish_version=self.processor_col.redfish_version,
            registries=None,
        )

    @mock.patch.object(processor, "Processor", autospec=True)
    def test_get_members(self, mock_system):
        members = self.processor_col.get_members()
        calls = [
            mock.call(
                self.processor_col._conn,
                "/redfish/v1/Systems/System1/Processors/CPU1",
                redfish_version=self.processor_col.redfish_version,
                registries=None,
            ),
            mock.call(
                self.processor_col._conn,
                "/redfish/v1/Systems/System1/Processors/CPU2",
                redfish_version=self.processor_col.redfish_version,
                registries=None,
            ),
        ]
        mock_system.assert_has_calls(calls)
        self.assertIsInstance(members, list)
        self.assertEqual(2, len(members))
